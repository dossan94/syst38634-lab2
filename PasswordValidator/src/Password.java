/*
* @author Anthony Doss, 50059
* This class validates passwords and it will be developed using TDD
*/
public class Password {
	 public static boolean isValidPass(String pass){
	        if(pass != null){
	            int length = pass.trim().length();
	            return length >= 8;
	        }
	        else {
	            return false;
	        }
	    }
	    public static boolean hasTwoDigits(String pass){
	        if(pass != null){
	            int digitnum = 0;
	           for (char c : pass.toCharArray()){
	               if (Character.isDigit(c)){
	                   digitnum++;
	               }
	           }
	          return digitnum >= 2;
	        }
	        else {
	            return false;
	        }
	    }
}
